﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetPalace.Models
{
    public class Pet
    {
        public Guid PetId { get; set; }
        public string PetName { get; set; }
        public int Hunger { get; set; }
        public int Happiness { get; set; }
    }
}
