﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetPalace.Models
{
    public class Food
    {
        public Guid FoodId { get; set; }
        public string FoodName { get; set; }
        public int FoodRate { get; set; }
        public int Qty { get; set; }
    }
}
