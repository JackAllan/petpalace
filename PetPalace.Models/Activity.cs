﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetPalace.Models
{
    public class Activity
    {
        public Guid ActivityId { get; set; }
        public string ActivityName { get; set; }
        public int ActivityEnergyRequired { get; set; }
        public int ActivityHappinessIncrease { get; set; }
    }
}
