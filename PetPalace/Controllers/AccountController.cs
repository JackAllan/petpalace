﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetPalace.Models;
using PetPalace.Services.Interfaces;

namespace PetPalace.Controllers
{
    [ApiController]
    [Route("accounts")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountServices _accountServices;

        public AccountController(IAccountServices accountServices)
        {
            _accountServices = accountServices;
        }

        [HttpGet]
        [Route("users")]
        public ActionResult<List<User>> GetUsers()
        {
            try
            {
                return Ok(_accountServices.GetUsers());
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("{userId}")]
        public ActionResult<User> GetUser(Guid userId)
        {
            try
            {
                return Ok(_accountServices.GetUser(userId));
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("{userId}/pets")]
        public ActionResult<IEnumerable<Pet>> GetUserPets(Guid userId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("A valid user ID has not been entered. Please try again.");
            }

            return Ok(_accountServices.GetUserPets(userId));
        }
    }
}
