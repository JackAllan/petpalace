﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PetPalace.Services.Interfaces;
using PetPalace.Models;

namespace PetPalace.Controllers
{
    [ApiController]
    [Route("hunger")]
    public class HungerController : ControllerBase
    {
        private readonly IFeedServices _feedServices;

        public HungerController(IFeedServices feedServices)
        {
            _feedServices = feedServices;
        }

        [HttpGet]
        [Route("food")]
        public ActionResult<List<Food>> GetFoodItems()
        {
            try
            {                
                return Ok(_feedServices.GetFood());
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        [Route("feed/{petid}/{foodid}")]
        public ActionResult<Pet> FeedPet(Guid petId, Guid foodId)
        {
            try
            {
                _feedServices.FeedPet(petId, foodId);
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest();
            }
        }
    }
}
