﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PetPalace.Services.Interfaces;
using PetPalace.Models;

namespace PetPalace.Controllers
{
    [ApiController]
    [Route("pet")]
    public class PetController : ControllerBase
    {
        private readonly IPetServices _petServices;        

        public PetController(IPetServices petServices)
        {
            _petServices = petServices;
        }

        [HttpPost]
        [Route("update")]
        public ActionResult<Pet> UpdatePet(Pet pet)
        {
            try
            {
                _petServices.UpdatePet(pet);
            }
            catch(Exception e)
            {
                return BadRequest(e.InnerException.ToString());
            }

            return Ok(pet);
        }

        [HttpDelete]
        [Route("delete/{petId}")]
        public IActionResult DeletePet(Guid petId)
        {
            try
            {
                _petServices.DeletePet(petId);
            }
            catch(Exception e)
            {
                return BadRequest(e.InnerException.ToString());
            }            
                
            return Ok();
        }
    }
}
