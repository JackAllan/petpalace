﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PetPalace.Models;
using PetPalace.Services.Interfaces;

namespace PetPalace.Controllers
{
    [ApiController]
    [Route("entertainment")]
    public class EntertainController : ControllerBase
    {
        private readonly IEntertainServices _entertainServices;
        
        public EntertainController(IEntertainServices entertainServices)
        {
            _entertainServices = entertainServices;
        }

        [HttpGet]
        [Route("activities")]
        public ActionResult<IEnumerable<Activity>> GetActivities()
        {
            try
            {
                return Ok(_entertainServices.GetActivities());
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("/activitydetail/{activityId}")]
        public ActionResult<Activity> GetActivity(Guid activityId)
        {
            try
            {
                return Ok(_entertainServices.GetActivity(activityId));
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("entertain/{petId}/{activityId}")]
        public ActionResult<Pet> EntertainPet(Guid userId, Guid petId, Guid activityId)
        {
            try
            {
                _entertainServices.EntertainPet(userId, petId, activityId);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }
    }
}
