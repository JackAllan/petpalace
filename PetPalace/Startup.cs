﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using PetPalace.Data;
using PetPalace.Mapper;
using PetPalace.Services;
using PetPalace.Services.Interfaces;

namespace PetPalace
{
    public class Startup
    {
        private readonly IConfiguration _config;

        public Startup(IConfiguration config)
        {
            _config = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<AppDbContext>(cfg => 
            {
                cfg.UseSqlServer(_config.GetConnectionString("PetPalaceConnectionString"));
            });

            services.AddTransient<IAccountServices, AccountServices>();
            services.AddTransient<IPetServices, PetServices>();
            services.AddTransient<IEntertainServices, EntertainServices>();
            services.AddTransient<IFeedServices, FeedServices>();

            services.AddAutoMapper(typeof(UserProfile));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Swashbuckle.AspNetCore.Swagger.Info
                    {
                        Version = "v1",
                        Title = "PetPalace",
                        Description = "Pet Palace API"
                    });
            });

            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    //c.RoutePrefix = "";
                    c.SwaggerEndpoint("v1/swagger.json", "PetPalace");                   
                });
            }

            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}
