﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PetPalace.Data.Entities;
using PetPalace.Models;

namespace PetPalace.Mapper
{
    public class PetProfile : Profile
    {
        public PetProfile()
        {
            CreateMap<Pet, PetEntity>()
                .ReverseMap();
        }
    }
}
