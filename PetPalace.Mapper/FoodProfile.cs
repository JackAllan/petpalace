﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PetPalace.Models;
using PetPalace.Data.Entities;

namespace PetPalace.Mapper
{
    public class FoodProfile : Profile
    {
        public FoodProfile()
        {
            CreateMap<Food, FoodEntity>()
                .ReverseMap();
        }
    }
}
