﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PetPalace.Services.Interfaces;
using PetPalace.Models;
using PetPalace.Data;
using PetPalace.Data.Entities;

namespace PetPalace.Services
{
    public class FeedServices : IFeedServices
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly IAccountServices _accountServices;
        private readonly IPetServices _petServices;

        public FeedServices(AppDbContext context, IMapper mapper, IAccountServices accountServices, IPetServices petServices)
        {
            _context = context;
            _mapper = mapper;
            _accountServices = accountServices;
            _petServices = petServices;
        }
        
        public IEnumerable<Food> GetFood()
        {
            var foodEntities = _context.Food
                .ToList();

            return _mapper.Map<IEnumerable<Food>>(foodEntities);
        }

        public Food GetFood(Guid foodId)
        {
            var foodEntity = _context.Food
                .Where(f => f.FoodId == foodId)
                .FirstOrDefault();

            return _mapper.Map<Food>(foodEntity);
        }

        public Pet FeedPet(Guid petId, Guid foodId)
        {
            var food = GetFood(foodId);
            var pet = _accountServices.GetUserPet(petId);

            pet.Hunger -= food.FoodRate;

            _petServices.UpdatePet(pet);

            return pet;
        }
    }
}
