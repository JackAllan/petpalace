﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PetPalace.Models;
using PetPalace.Data;
using PetPalace.Services.Interfaces;

namespace PetPalace.Services
{
    public class AccountServices : IAccountServices
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public AccountServices(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<User> GetUsers()
        {
            var users = _context.Users
                .ToList();

            return _mapper.Map<IEnumerable<User>>(users);
        }

        public User GetUser(Guid userId)
        {
            var user = _context.Users
                .Where(u => u.UserId == userId)
                .FirstOrDefault();

            return _mapper.Map<User>(user);
        }

        public IEnumerable<Pet> GetUserPets(Guid userId)
        {
            var pets = _context.Pets
                .Where(p => p.UserId == userId)
                .ToList();

            return _mapper.Map<IEnumerable<Pet>>(pets);
        }

        public Pet GetUserPet(Guid petId)
        {
            var pet = _context.Pets
                .Where(p => p.PetId == petId)
                .FirstOrDefault();

            return _mapper.Map<Pet>(pet);
        }
    }
}
