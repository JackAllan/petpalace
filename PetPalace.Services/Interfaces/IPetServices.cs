﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetPalace.Models;
using PetPalace.Data.Entities;


namespace PetPalace.Services.Interfaces
{
    public interface IPetServices
    {
        void UpdatePet(Pet pet);
        void DeletePet(Guid petId);
    }
}
