﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetPalace.Models;

namespace PetPalace.Services.Interfaces
{
    public interface IFeedServices
    {        
        IEnumerable<Food> GetFood();
        Food GetFood(Guid foodId);
        Pet FeedPet(Guid petId, Guid foodId);
    }
}
