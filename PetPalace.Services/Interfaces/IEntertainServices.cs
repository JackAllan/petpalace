﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetPalace.Models;
using PetPalace.Data.Entities;

namespace PetPalace.Services.Interfaces
{
    public interface IEntertainServices
    {
        IEnumerable<Activity> GetActivities();
        Activity GetActivity(Guid activityId);
        void EntertainPet(Guid userId, Guid petId, Guid activityId);
    }
}
