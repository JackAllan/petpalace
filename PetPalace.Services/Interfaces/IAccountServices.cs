﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetPalace.Models;

namespace PetPalace.Services.Interfaces
{
    public interface IAccountServices
    {
        IEnumerable<User> GetUsers();
        User GetUser(Guid userid);
        IEnumerable<Pet> GetUserPets(Guid userId);
        Pet GetUserPet(Guid petId);
    }
}
