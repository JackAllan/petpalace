﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PetPalace.Services.Interfaces;
using PetPalace.Models;
using PetPalace.Data;
using PetPalace.Data.Entities;

namespace PetPalace.Services
{
    public class EntertainServices : IEntertainServices
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public EntertainServices(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<Activity> GetActivities()
        {
            var entities = _context.Activities
                .ToList();

            return _mapper.Map<IEnumerable<Activity>>(entities);
        }

        public Activity GetActivity(Guid activityId)
        {
            var entity = _context.Activities
                .Where(a => a.ActivityId == activityId)
                .ToList();

            return _mapper.Map<Activity>(entity);                
        }

        public void EntertainPet(Guid userId, Guid petId, Guid activityId)
        {
            var user = _context.Users
                .Where(u => u.UserId == userId)
                .FirstOrDefault();

            var pet = _context.Pets
                .Where(p => p.PetId == petId)
                .FirstOrDefault();

            var activity = _context.Activities
                .Where(a => a.ActivityId == activityId)
                .FirstOrDefault();

            if(user.Energy < activity.ActivityEnergyRequired)
            {
                throw new Exception("You do not have enough energy to perform this activity. Please try again later.");
            }

            //TODO: Add pet max happiness barrier

            pet.PetHappiness += activity.ActivityHappinessIncrease;
            _context.Update(pet);

            user.Energy -= activity.ActivityEnergyRequired;
            _context.Update(user);

            _context.SaveChanges();
        }
    }
}
