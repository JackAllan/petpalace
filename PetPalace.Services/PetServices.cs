﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PetPalace.Models;
using PetPalace.Data;
using PetPalace.Data.Entities;
using PetPalace.Services.Interfaces;

namespace PetPalace.Services
{
    public class PetServices : IPetServices
    {
        private readonly AppDbContext _context;
        private readonly IAccountServices _accountServices;
        private readonly IMapper _mapper;

        public PetServices(AppDbContext context, IAccountServices accountServices, IMapper mapper)
        {
            _context = context;
            _accountServices = accountServices;
            _mapper = mapper;
        }

        public void UpdatePet(Pet pet)
        {
            _context.Update(_mapper.Map<PetEntity>(pet));
        }

        public void DeletePet(Guid petId)
        {
            var pet = _accountServices.GetUserPet(petId);

            _context.Remove(_mapper.Map<PetEntity>(pet));
        }
    }
}
