﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetPalace.Data.Entities
{
    public class EnergyEntity
    {
        [Key]
        public Guid UserId { get; set; }
        public int EnergyAmount { get; set; }
        public int EnergyRechargeRate { get; set; }
        public int FoodRechargeRate { get; set; }
    }
}
