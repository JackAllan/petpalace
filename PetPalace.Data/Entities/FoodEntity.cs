﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetPalace.Data.Entities
{
    public class FoodEntity
    {
        [Key]
        public Guid FoodId { get; set; }
        public string FoodName { get; set; }
        public int FoodRate { get; set; }
        public int Qty { get; set; }
    }
}
