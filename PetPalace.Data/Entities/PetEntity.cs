﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetPalace.Data.Entities
{
    public class PetEntity
    {
        [Key]
        public Guid UserId { get; set; }
        public Guid PetId { get; set; }
        public string PetName { get; set; }
        public int PetHunger { get; set; }
        public int PetHappiness { get; set; }
    }
}
