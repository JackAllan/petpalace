﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetPalace.Data.Entities
{
    public class ActivityEntity
    {
        [Key]
        public Guid ActivityId { get; set; }
        public string ActivityName { get; set; }
        public int ActivityEnergyRequired { get; set; }
        public int ActivityHappinessIncrease { get; set; }
    }
}
