﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PetPalace.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Activities",
                columns: table => new
                {
                    ActivityId = table.Column<Guid>(nullable: false),
                    ActivityName = table.Column<string>(nullable: true),
                    ActivityEnergyRequired = table.Column<int>(nullable: false),
                    ActivityHappinessIncrease = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activities", x => x.ActivityId);
                });

            migrationBuilder.CreateTable(
                name: "Food",
                columns: table => new
                {
                    FoodId = table.Column<Guid>(nullable: false),
                    FoodName = table.Column<string>(nullable: true),
                    FoodRate = table.Column<int>(nullable: false),
                    Qty = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Food", x => x.FoodId);
                });

            migrationBuilder.CreateTable(
                name: "Pets",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    PetId = table.Column<Guid>(nullable: false),
                    PetName = table.Column<string>(nullable: true),
                    PetHunger = table.Column<int>(nullable: false),
                    PetHappiness = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pets", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Currency = table.Column<double>(nullable: false),
                    Energy = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.InsertData(
                table: "Activities",
                columns: new[] { "ActivityId", "ActivityEnergyRequired", "ActivityHappinessIncrease", "ActivityName" },
                values: new object[] { new Guid("9275a0f9-b0df-4d78-9ca6-5d487e4f5838"), 1, 1, "SeedActivity" });

            migrationBuilder.InsertData(
                table: "Food",
                columns: new[] { "FoodId", "FoodName", "FoodRate", "Qty" },
                values: new object[] { new Guid("717c5592-9636-4541-a5f3-c961e0844cc4"), "SeedFood", 1, 1 });

            migrationBuilder.InsertData(
                table: "Pets",
                columns: new[] { "UserId", "PetHappiness", "PetHunger", "PetId", "PetName" },
                values: new object[] { new Guid("51e39e0c-a000-4d59-9e97-2d57a6d6b5e9"), 0, 0, new Guid("805f6539-2f38-45aa-b4db-9535d7f3f5c6"), "SeedPet" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Currency", "Energy", "FirstName", "LastName" },
                values: new object[] { new Guid("51e39e0c-a000-4d59-9e97-2d57a6d6b5e9"), 10.01, 10, "Seed", "User" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Activities");

            migrationBuilder.DropTable(
                name: "Food");

            migrationBuilder.DropTable(
                name: "Pets");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
