﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PetPalace.Data.Entities;

namespace PetPalace.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<PetEntity> Pets { get; set; }
        public DbSet<FoodEntity> Food { get; set; }
        public DbSet<ActivityEntity> Activities { get; set; }

        private Guid UserIdNew { get; set; }
        private Guid PetIdNew { get; set; }
        private Guid FoodIdNew { get; set; }
        private Guid ActivityIdNew { get; set; }

        private void GenerateIds()
        {
            UserIdNew = Guid.NewGuid();
            PetIdNew = Guid.NewGuid();
            FoodIdNew = Guid.NewGuid();
            ActivityIdNew = Guid.NewGuid();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            GenerateIds();

            modelBuilder.Entity<UserEntity>()
                .HasData(
                    new
                    {
                        UserId = UserIdNew,
                        FirstName = "Seed",
                        LastName = "User",
                        Currency = 10.01,
                        Energy = 10
                    }
                );

            modelBuilder.Entity<PetEntity>()
                .HasData(
                    new PetEntity
                    {
                        UserId = UserIdNew,
                        PetId = PetIdNew,
                        PetName = "SeedPet",
                        PetHappiness = 0,
                        PetHunger = 0
                    }
                );

            modelBuilder.Entity<FoodEntity>()
                .HasData(
                    new FoodEntity
                    {
                        FoodId = FoodIdNew,
                        FoodName = "SeedFood",
                        FoodRate = 1,
                        Qty = 1
                    }
                );

            modelBuilder.Entity<ActivityEntity>()
                .HasData(
                    new ActivityEntity
                    {
                        ActivityId = ActivityIdNew,
                        ActivityName = "SeedActivity",
                        ActivityEnergyRequired = 1,
                        ActivityHappinessIncrease = 1
                    }
                );
        }
    }
}
